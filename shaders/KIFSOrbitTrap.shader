shader_type spatial;
render_mode cull_front, depth_draw_always;

uniform mat4 objectTransform;
uniform mat4 shipTransform;
uniform bool shipShadow = false;

uniform float surfaceDistance = 0.002;

uniform float maxDistance = 1e10;

uniform float objectScale = 1.0;

uniform vec3 sunRayDirection;
uniform float sunSize = 0.3;

uniform vec3 axis = vec3(1.0, 4.0, 2.0);
uniform float angle = 0.4454;
uniform int iterations = 20;
uniform float smoothing = 0.05;

uniform float fScale=1.25;
uniform vec3 vOffset = vec3(-1.0,-2.0,-0.2);

uniform vec4 albedo: hint_color;

uniform vec4 trap1_albedo: hint_color;
uniform vec3 trap1;
uniform float trap1scale = 1.0;

uniform vec4 trap2_albedo: hint_color;
uniform vec3 trap2;
uniform float trap2scale = 1.0;

uniform float roughness = 0.9;
uniform float specular = 0.0;
uniform float metallic = 0.0;

varying vec3 sunDirection;
varying mat3 m;
// mat3 from quaternion
mat3 SetRot( in vec4 q )
{
	vec4 qSq = q * q;
	float xy2 = q.x * q.y * 2.0;
	float xz2 = q.x * q.z * 2.0;
	float yz2 = q.y * q.z * 2.0;
	float wx2 = q.w * q.x * 2.0;
	float wy2 = q.w * q.y * 2.0;
	float wz2 = q.w * q.z * 2.0;
 
	return mat3 (	
     vec3(qSq.w + qSq.x - qSq.y - qSq.z, xy2 - wz2, xz2 + wy2),
     vec3(xy2 + wz2, qSq.w - qSq.x + qSq.y - qSq.z, yz2 - wx2),
     vec3(xz2 - wy2, yz2 + wx2, qSq.w - qSq.x - qSq.y + qSq.z) );
}

// mat3 from axis / angle
mat3 SetRot2( vec3 vAxis, float fAngle )
{	
	return SetRot( vec4(normalize(vAxis) * sin(fAngle), cos(fAngle)) );
}

void vertex(){
	sunDirection = normalize(sunRayDirection);
	m = SetRot2(axis, angle);
}

// SHIP SDF /////////////////////////////////////////
float sdSphere( vec3 p, float s )
{
    return length(p)-s;
}

float sdEllipsoid( in vec3 p, in vec3 r ) // approximated
{
    float k0 = length(p/r);
    float k1 = length(p/(r*r));
    return k0*(k0-1.0)/k1;
}

// vertical
float sdCone( in vec3 p, in vec2 c, float h )
{
    vec2 q = h*vec2(c.x,-c.y)/c.y;
    vec2 w = vec2( length(p.xz), p.y );
    
	vec2 a = w - q*clamp( dot(w,q)/dot(q,q), 0.0, 1.0 );
    vec2 b = w - q*vec2( clamp( w.x/q.x, 0.0, 1.0 ), 1.0 );
    float k = sign( q.y );
    float d = min(dot( a, a ),dot(b, b));
    float s = max( k*(w.x*q.y-w.y*q.x),k*(w.y-q.y)  );
	return sqrt(d)*sign(s);
}

float sdTorus( vec3 p, vec2 t )
{
  vec2 q = vec2(length(p.xz)-t.x,p.y);
  return length(q)-t.y;
}

float sdCapsule( vec3 p, vec3 a, vec3 b, float r )
{
  vec3 pa = p - a, ba = b - a;
  float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
  return length( pa - ba*h ) - r;
}

float sdCappedCone(vec3 p, vec3 a, vec3 b, float ra, float rb)
{
    float rba  = rb-ra;
    float baba = dot(b-a,b-a);
    float papa = dot(p-a,p-a);
    float paba = dot(p-a,b-a)/baba;
    float x = sqrt( papa - paba*paba*baba );
    float cax = max(0.0,x-((paba<0.5)?ra:rb));
    float cay = abs(paba-0.5)-0.5;
    float k = rba*rba + baba;
    float f = clamp( (rba*(x-ra)+paba*baba)/k, 0.0, 1.0 );
    float cbx = x-ra - f*rba;
    float cby = paba - f;
    float s = (cbx < 0.0 && cay < 0.0) ? -1.0 : 1.0;
    return s*sqrt( min(cax*cax + cay*cay*baba,
                       cbx*cbx + cby*cby*baba) );
}

float opSubtract( float d1, float d2 ) {
	 return max(-d1,d2); 
}

vec3 opElongate(in vec3 p, in vec3 h )
{
    return p - clamp( p, -h, h );
}

float opSmoothUnion( float d1, float d2, float k ) {
    float h = clamp( 0.5 + 0.5*(d2-d1)/k, 0.0, 1.0 );
    return mix( d2, d1, h ) - k*h*(1.0-h); 

}

float shipBody( in vec3 p) {
	return opSmoothUnion(
		sdEllipsoid(p, vec3(0.3, 0.4, 0.3)),
		sdCapsule(vec3(abs(p.x), p.y, abs(p.z)), vec3(0.2, 0.0, 0.2), vec3(0.28, -0.1, 0.28), 0.015),
		0.02
	);
}

float shipEngine( in vec3 p){
	return opSubtract(
		sdCone(p + vec3(0.0, 0.34, 0.0), vec2(0.6, 1.0), 0.25),
		sdCone(p + vec3(0.0, 0.42, 0.0), vec2(0.40, 1.0), 0.05) - 0.07
	);
}

// ring and landing gear
float shipRing( in vec3 p){
	return min(
		sdTorus(opElongate(p + vec3(0, 0.05, 0), vec3(0, 0.05, 0)), vec2(0.4, 0.013)),
		sdCappedCone(vec3(abs(p.x), p.y, abs(p.z)), vec3(0.1, -0.3, 0.1), vec3(0.17, -0.5, 0.17), 0.02, 0.01)
	);
}

float ship(in vec3 p){
	return min(
		min(
			shipBody(p),
			shipEngine(p)
		),
		shipRing(p)
	);
}
// SHIP SDF /////////////////////////////////////////

float maxcomp(in vec3 p ) { return max(p.x,max(p.y,p.z));}
float sdBox( vec3 p, vec3 b )
{
  vec3  di = abs(p) - b;
  float mc = maxcomp(di);
  return min(mc,length(max(di,0.0)));
}


// Thing!
float map( in vec3 vPos )
{
	vPos = (inverse(objectTransform) * vec4(vPos, 1.0)).xyz;
	vPos /= objectScale;
	float fTotalScale = 1.0;
	for(int i=0; i<iterations; i++)
	{	
		vPos.xyz = abs(vPos.xyz);
		vPos *= fScale;
		fTotalScale *= fScale;
		vPos += vOffset;
		vPos.xyz = (vPos.xyz) * m;
		
		float fCurrDist = length(vPos.xyz) * fTotalScale;
		//float fCurrDist = max(max(vPos.x, vPos.y), vPos.z) * fTotalScale;
		//float fCurrDist = dot(vPos.xyz, vPos.xyz);// * fTotalScale;
	}

	float l = length(vPos.xyz) / fTotalScale;
	
	float fDist = l - smoothing;
	return fDist * objectScale;
}

// Thing!
vec2 material( in vec3 vPos )
{
	vPos = (inverse(objectTransform) * vec4(vPos, 1.0)).xyz;
	vPos /= objectScale;
	float fTotalScale = 1.0;
	float trap1dist = 1e10;
	float trap2dist = 1e10;
	for(int i=0; i<iterations; i++)
	{	
		vPos.xyz = abs(vPos.xyz);
		vPos *= fScale;
		fTotalScale *= fScale;
		vPos += vOffset;
		vPos.xyz = (vPos.xyz) * m;
		
		vec3 trap1vec = vPos - trap1;
		vec3 trap2vec = vPos - trap2;
		trap1dist = min(trap1dist, maxcomp(trap1vec) * fTotalScale);
		trap2dist = min(trap2dist, maxcomp(trap2vec) * fTotalScale);
		//float fCurrDist = max(max(vPos.x, vPos.y), vPos.z) * fTotalScale;
		//float fCurrDist = dot(vPos.xyz, vPos.xyz);// * fTotalScale;
	}
	
	return vec2(trap1dist, trap2dist) * vec2(trap1scale, trap2scale);
}


float mapWithShip(in vec3 p){
	vec3 ship_p = (inverse(shipTransform) * vec4(p, 1.0)).xyz;
	return min(map(p), ship(ship_p));
}

bool intersect( in vec3 ro, in vec3 rd, out float dist )
{
	float h = 1.0;
	dist = 0.0;
    for( int i=0; i<128; i++ )
    {
		if( h < surfaceDistance * 0.01) return true;
		h = map(ro + rd * dist);
        dist += h;
		if( dist > maxDistance) return false;
    }
	return true;
}

float softshadow( in vec3 ro, in vec3 rd, float mint, float k )
{
    float res = 1.0;
    float t = mint;
	float h = 1.0;
    for( int i=0; i<32; i++ )
    {
        if(shipShadow){
			h = mapWithShip(ro + rd*t);
		} else {
			h = map(ro + rd*t);
		}
        res = min( res, k*h/t );
		t += clamp( h, 0.005, 0.1 );
    }
    return clamp(res,0.0,1.0);
}

vec3 calcNormal(in vec3 pos)
{
    vec3  eps = vec3(.001,0.0,0.0);
    vec3 normal;
	float center = map(pos);
    normal.x = map(pos+eps.xyy) - center;
    normal.y = map(pos+eps.yxy) - center;
    normal.z = map(pos+eps.yyx) - center;
    return normalize(normal);
}

float calcAO( in vec3 pos, in vec3 nor )
{
	float occ = 0.0;
    float sca = 1.0;
    for( int i=0; i<5; i++ )
    {
        float h = 0.01 + 0.12*float(i)/4.0;
        float d;
		if(shipShadow){
			d = mapWithShip( pos + h*nor );
		}else{
			d = map( pos + h*nor );
		}
        occ += (h-d)*sca;
        sca *= 0.95;
        if( occ>0.35 ) break;
    }
    return clamp( 1.0 - 3.0*occ, 0.0, 1.0 ) * (0.5+0.5*nor.y);
}

void fragment(){
	vec3 ray_start_pos = (CAMERA_MATRIX * vec4(VERTEX, 1.0)).xyz;
	vec3 camera_pos = (CAMERA_MATRIX * vec4(0.0,0.0,0.0,1.0)).xyz;
	
	vec3 ray_dir = normalize(ray_start_pos - camera_pos) * 0.9;

	
	// Perform raymarching
	float dist = 0.0;
	bool hit = intersect(camera_pos, ray_dir, dist);

	// Draw objects or background/glow
	if(hit){
		vec3  pos = camera_pos + dist * ray_dir;
        vec3  normal = calcNormal(pos);
		
        float occ = calcAO(pos, normal);
		float shadow = softshadow( pos, -sunDirection, 0.01, 1.0 / sunSize);
		
		vec2 mat = material(pos);
		ALBEDO = mix(albedo.rgb, trap1_albedo.rgb, mat.x) + trap2_albedo.rgb * mat.y;
		ROUGHNESS = roughness;
		SPECULAR = specular;
		METALLIC = metallic;
		AO = occ + shadow;
		AO_LIGHT_AFFECT = 1.0;
		NORMAL = (INV_CAMERA_MATRIX * vec4(normal, 0.0)).xyz;
		// WARNING: Horrific hack ahead
		// Depth should be transformed with the projection matrix in a nonlinear
		// way, but I couldn't figure that out, so this should do I guess.
		DEPTH = dist / 1000.0;
	}else{
		ALPHA = 0.0;
		DEPTH = 1.0;
	}
}