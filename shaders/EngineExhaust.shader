shader_type spatial;
render_mode blend_add,cull_disabled,diffuse_burley,specular_schlick_ggx,skip_vertex_transform;

uniform vec4 albedo : hint_color;
uniform vec4 emission_start : hint_color;
uniform vec4 emission_end : hint_color;
uniform float emission_energy;

varying float dist;

void vertex() {
	dist = -VERTEX.y + 0.5;
	VERTEX = (MODELVIEW_MATRIX * vec4(VERTEX, 1.0)).xyz;
	NORMAL = (MODELVIEW_MATRIX * vec4(NORMAL, 0.0)).xyz;
}

void fragment() {
	vec2 base_uv = UV;
	ALBEDO = albedo.rgb;
	ROUGHNESS = 1.0;
	EMISSION = mix(emission_start.rgb, emission_end.rgb, dist);
	ALPHA = mix(emission_energy, 0, pow(dist, 0.5));
//	if(dist < 0.01){
//		ALPHA = 0.0;
//	}
	
	DEPTH = length(VERTEX) / 1000.0;
}
