shader_type spatial;
render_mode unshaded, cull_front, depth_test_disable;

uniform float surfaceDistance = 0.002;

uniform float maxDistance = 1e10;

uniform vec3 sunRayDirection;

uniform vec3 axis = vec3(1.0, 4.0, 2.0);
uniform float angle = 0.4454;
uniform int iterations = 20;
uniform float smoothing = 0.05;

varying vec3 sunDirection;
varying mat3 m;
// mat3 from quaternion
mat3 SetRot( in vec4 q )
{
	vec4 qSq = q * q;
	float xy2 = q.x * q.y * 2.0;
	float xz2 = q.x * q.z * 2.0;
	float yz2 = q.y * q.z * 2.0;
	float wx2 = q.w * q.x * 2.0;
	float wy2 = q.w * q.y * 2.0;
	float wz2 = q.w * q.z * 2.0;
 
	return mat3 (	
     vec3(qSq.w + qSq.x - qSq.y - qSq.z, xy2 - wz2, xz2 + wy2),
     vec3(xy2 + wz2, qSq.w - qSq.x + qSq.y - qSq.z, yz2 - wx2),
     vec3(xz2 - wy2, yz2 + wx2, qSq.w - qSq.x - qSq.y + qSq.z) );
}

// mat3 from axis / angle
mat3 SetRot2( vec3 vAxis, float fAngle )
{	
	return SetRot( vec4(normalize(vAxis) * sin(fAngle), cos(fAngle)) );
}

void vertex(){
	sunDirection = normalize(sunRayDirection);
	m = SetRot2(axis, angle);
}

float maxcomp(in vec3 p ) { return max(p.x,max(p.y,p.z));}
float sdBox( vec3 p, vec3 b )
{
  vec3  di = abs(p) - b;
  float mc = maxcomp(di);
  return min(mc,length(max(di,0.0)));
}

const float fScale=1.25;
const vec3 vOffset = vec3(-1.0,-2.0,-0.2);

// Thing!
float map( in vec3 vPos )
{
	float fTotalScale = 1.0;
	for(int i=0; i<iterations; i++)
	{	
		vPos.xyz = abs(vPos.xyz);
		vPos *= fScale;
		fTotalScale *= fScale;
		vPos += vOffset;
		vPos.xyz = (vPos.xyz) * m;
		
		float fCurrDist = length(vPos.xyz) * fTotalScale;
		//float fCurrDist = max(max(vPos.x, vPos.y), vPos.z) * fTotalScale;
		//float fCurrDist = dot(vPos.xyz, vPos.xyz);// * fTotalScale;
	}

	float l = length(vPos.xyz) / fTotalScale;
	
	float fDist = l - smoothing;
	return fDist;
}

bool intersect( in vec3 ro, in vec3 rd, out float dist )
{
	float h = 1.0;
	dist = 0.0;
    for( int i=0; i<128; i++ )
    {
		if( h < surfaceDistance) return true;
		h = map(ro + rd * dist);
        dist += h;
		if( dist > maxDistance) return false;
    }
	return true;
}

float softshadow( in vec3 ro, in vec3 rd, float mint, float k )
{
    float res = 1.0;
    float t = mint;
	float h = 1.0;
    for( int i=0; i<32; i++ )
    {
        h = map(ro + rd*t);
        res = min( res, k*h/t );
		t += clamp( h, 0.005, 0.1 );
    }
    return clamp(res,0.0,1.0);
}

vec3 calcNormal(in vec3 pos)
{
    vec3  eps = vec3(.001,0.0,0.0);
    vec3 normal;
	float center = map(pos);
    normal.x = map(pos+eps.xyy) - center;
    normal.y = map(pos+eps.yxy) - center;
    normal.z = map(pos+eps.yyx) - center;
    return normalize(normal);
}

float calcAO( in vec3 pos, in vec3 nor )
{
	float occ = 0.0;
    float sca = 1.0;
    for( int i=0; i<5; i++ )
    {
        float h = 0.01 + 0.12*float(i)/4.0;
        float d = map( pos + h*nor );
        occ += (h-d)*sca;
        sca *= 0.95;
        if( occ>0.35 ) break;
    }
    return clamp( 1.0 - 3.0*occ, 0.0, 1.0 ) * (0.5+0.5*nor.y);
}

void fragment(){
	vec3 ray_start_pos = (CAMERA_MATRIX * vec4(VERTEX, 1.0)).xyz;
	vec3 camera_pos = (CAMERA_MATRIX * vec4(0.0,0.0,0.0,1.0)).xyz;
	
	vec3 ray_dir = normalize(ray_start_pos - camera_pos) * 0.9;

	
	// Perform raymarching
	float dist = 0.0;
	bool hit = intersect(camera_pos, ray_dir, dist);

	// Draw objects or background/glow
	if(hit){
		vec3  pos = camera_pos + dist * ray_dir;
        vec3  normal = calcNormal(pos);
		
        float occ = calcAO(pos, normal);
		float sha = softshadow( pos, sunDirection, 0.01, 64.0 );

		float dif = max(0.1 + 0.9*dot(normal,sunDirection),0.0);
		float sky = 0.5 + 0.5*normal.y;
        float bac = max(0.4 + 0.6*dot(normal,vec3(-sunDirection.x,sunDirection.y,-sunDirection.z)),0.0);

        vec3 lin  = vec3(0.0);
        lin += 1.00*dif*vec3(1.10,0.85,0.60)*sha;
        lin += 0.50*sky*vec3(0.10,0.20,0.40)*occ;
        lin += 0.10*bac*vec3(1.00,1.00,1.00)*(0.5+0.5*occ);
        lin += 0.25*occ*vec3(0.15,0.17,0.20);

        vec3 matcol = vec3(0.5, 0.5, 0.5);
        ALBEDO = pow( matcol * lin , vec3(0.4545) );
	}else{
		ALPHA = 0.0;
		DEPTH = 1.0;
	}
}