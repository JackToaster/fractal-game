shader_type spatial;
render_mode blend_mix,depth_draw_always,cull_disabled,unshaded;
uniform vec4 albedo : hint_color;

void fragment() {
	ALBEDO = albedo.rgb;
	ALPHA = albedo.a;
	DEPTH = length(VERTEX) / 1000.0;
}
