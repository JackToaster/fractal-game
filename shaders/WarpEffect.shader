shader_type canvas_item;

uniform vec4 color: hint_color;
uniform float alpha;
const float PI = 3.1415928535;

float vDrop(vec2 uv,float t)
{
    uv.x = uv.x*256.0;						// H-Count
    float dx = fract(uv.x);
    uv.x = floor(uv.x);
    uv.y *= 0.05;							// stretch
    float o=sin(uv.x*215.4);				// offset
    float s=cos(uv.x*33.1)*.3 +.7;			// speed
    float trail = mix(95.0,15.0,s);			// trail length
    float yv = fract(uv.y + t*s + o) * trail;
    yv = 1.0/yv;
    yv = smoothstep(0.0,1.0,yv*yv);
    yv = sin(yv*PI)*(s*5.0);
    float d2 = sin(dx*PI);
    return yv*(d2*d2);
}

void fragment()
{
    vec2 p = SCREEN_UV - vec2(0.5);
    float d = length(p)+0.1;
	p = vec2(atan(p.x, p.y) / PI, 2.5 / d);
    
    float t =  TIME*0.4;
    
    vec3 col = vec3(10.0) * vDrop(p,t) * d * d;
	COLOR = vec4(col, alpha) + color;
}
