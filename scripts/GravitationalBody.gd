extends RigidBody

export(float) var radius = 0

onready var other_bodies: Array = get_tree().get_nodes_in_group("GravitationalBody")

# Newton's law of universal gravitation
# F = GMm/r^2
func _newtonian_gravity(m1: float, m2: float, r: float):
	return GlobalSettings.gravitational_constant * m1 * m2 / (r * r)

func _physics_process(_delta):
	for body in other_bodies:
		if body == self:
			continue
		var offset: Vector3 = body.global_transform.origin - global_transform.origin
		var force: float = _newtonian_gravity(mass, body.mass, offset.length())
		
		var force_vector: Vector3 = offset.normalized() * force
		add_central_force(force_vector)
