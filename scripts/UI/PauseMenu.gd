extends PopupMenu

export(String, FILE) var menu_scene

var old_timestate

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_Apply_pressed():
	hide()
	call_deferred("popup")

func _on_Close_pressed():
	hide()

func _unhandled_input(_event):
	if Input.is_action_pressed("ui_cancel"):
		popup()


func _on_PopupMenu_about_to_show():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().paused = true
	old_timestate = GlobalSettings.game_root.time_state
	GlobalSettings.game_root.set_time_state(Game.TimeState.frozen)


func _on_PopupMenu_popup_hide():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().paused = false
	GlobalSettings.game_root.set_time_state(old_timestate)


func _on_ExitToMenu_pressed():
	$WarpEffect.fade_in()
	$WarpEffect.visible = true


func _on_WarpEffect_fade_in_complete():
	get_tree().change_scene(menu_scene)
