extends Control

export(String, FILE) var game_scene

func _ready():
	$WarpEffect.fade_out()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_play_pressed():
	$WarpEffect.visible = true
	$WarpEffect.fade_in()


func _on_WarpEffect_fade_in_complete():
	get_tree().change_scene(game_scene)


func _on_WarpEffect_fade_out_complete():
	$WarpEffect.visible = false
