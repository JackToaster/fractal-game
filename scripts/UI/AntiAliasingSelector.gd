extends OptionButton


const options = [
	"Disabled",
	"2x",
	"4x",
	"8x",
	"16x"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	for option in options:
		add_item(option)
	add_item("Select")
	set_item_disabled(5, true)
	select(5)

func apply_setting():
	match selected:
		0:
			get_viewport().msaa = Viewport.MSAA_DISABLED
		1:
			get_viewport().msaa = Viewport.MSAA_2X
		2:
			get_viewport().msaa = Viewport.MSAA_4X
		3:
			get_viewport().msaa = Viewport.MSAA_8X
		4:
			get_viewport().msaa = Viewport.MSAA_16X
