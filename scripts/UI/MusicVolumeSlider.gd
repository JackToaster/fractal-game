extends HSlider

export(String) var music_bus = "Music"

export(Curve) var volume_curve

onready var bus_idx = AudioServer.get_bus_index(music_bus)

func _on_ResolutionSlider_value_changed(value):
	var volume = volume_curve.interpolate(value)
	AudioServer.set_bus_volume_db(bus_idx, volume)
	
