extends OptionButton

const resolutions = [
	"Full",
	"Half",
	"Quarter",
	"Eighth"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(len(resolutions)):
		var resolution = resolutions[i]
		add_item(resolution, i)
	
	add_item("Select")
	set_item_disabled(4, true)
	select(4)

func apply_setting():
	if selected == 4:
		return
	var size = resolutions[selected]
	var shrink
	match size:
		"Full": shrink = 1.0
		"Half": shrink = 2.0
		"Quarter": shrink = 4.0
		"Eighth": shrink = 8.0
		_:
			return
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_DISABLED,SceneTree.STRETCH_ASPECT_IGNORE, Vector2(320, 240), shrink)
