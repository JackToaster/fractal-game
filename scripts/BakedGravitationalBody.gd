extends Spatial

export(float) var mass
export(float) var radius = 0
export(Vector3) var linear_vel
export(Vector3) var angular_vel

onready var initial_xform: Transform = global_transform
onready var initial_rotation: Quat = Quat(initial_xform.basis)
onready var angular_vel_quat: Quat = Quat(angular_vel)

var baked_curve: Vector3Curve = Vector3Curve.new()

onready var bake_acceleration: Vector3 = Vector3.ZERO
onready var bake_velocity: Vector3 = linear_vel
onready var bake_position: Vector3 = initial_xform.origin

var max_timestep = 1.0

func set_max_timestep(time: float):
	max_timestep = time

func save_bake_state(time: float):
	baked_curve.add_point(time, bake_position)

func integrate_position(delta: float):
	bake_position += bake_velocity * delta + bake_acceleration * bake_acceleration * 0.5 * delta
	bake_velocity += delta * bake_acceleration

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func interpolate(time: float):
	var curve_time = time / max_timestep
	global_transform.origin = baked_curve.interpolate(curve_time)
	
	var rotation: Quat = Quat.IDENTITY.slerpni(angular_vel_quat, time)
	global_transform.basis = Basis(rotation * initial_rotation)

func get_interpolated_position(time: float):
	var curve_time = time / max_timestep
	return baked_curve.interpolate(curve_time)
