extends "res://scripts/raymarched/RayMarchedGeom.gd"


export(float) var radius


func set_fractal_parameters():
	material.set_shader_param("radius", radius)

func map(p: Vector3):
	return len(global_transform.origin - p) - radius
