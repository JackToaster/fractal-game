tool
extends MeshInstance

#export(NodePath) var sun_nodepath
#export(NodePath) var ship_nodepath


onready var material: ShaderMaterial = get_surface_material(0)
var ship = null
var sun: SunLight = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pause_mode = PAUSE_MODE_PROCESS
	
	var ships = get_tree().get_nodes_in_group("Ship")
	if len(ships) > 0:
		ship = ships[0]
	
	var suns = get_tree().get_nodes_in_group("Sun")
	if len(suns) > 0:
		sun = suns[0]
	
	update_material_params()
	if sun == null:
		push_warning("Sun nodepath not set, shadows will not work.")
	if ship == null:
		push_warning("Ship nodepath not set, ship shadows will be disabled")
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	update_material_params()

func update_material_params():
	set_material_sun()
	set_material_tranform()
	set_material_ship_shadow()
	set_fractal_parameters()

func set_material_sun():
	if sun == null:
		return
	var sun_offset: Vector3 = global_transform.origin - sun.global_transform.origin
	var sun_dist = sun_offset.length()
	if sun_dist == 0:
		sun_dist = 0.1
	var sun_size = sun.size / sun_dist
	
	material.set_shader_param("sunRayDirection", sun_offset)
	material.set_shader_param("sunSize", sun_size)

func set_material_tranform():
	material.set_shader_param("objectTransform", global_transform)

func set_material_ship_shadow():
	if ship == null:
		material.set_shader_param("shipShadow", false)
		#print("ship is null")
	else:
		material.set_shader_param("shipShadow", true)
		#print(ship.global_transform)
		material.set_shader_param("shipTransform", ship.global_transform)

func set_fractal_parameters():
	pass

func map(_p: Vector3) -> float:
	return 9999.0
