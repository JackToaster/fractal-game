tool
extends Area

# Called when the node enters the scene tree for the first time.
func _ready():
	$MengerKIFS.material = $MengerKIFS.material.duplicate()
	$MengerKIFS.set_surface_material(0, $MengerKIFS.material)
	$AnimationPlayer.play("pulse")

func reset():
	$AnimationPlayer.play("pulse")
	$CollisionShape.disabled = false

signal objective_collected
func collect():
	$AnimationPlayer.play("fadeout")
	$AudioStreamPlayer3D.pitch_scale = rand_range(0.75, 1.5)
	$AudioStreamPlayer3D.play()
	$CollisionShape.disabled = true
	emit_signal("objective_collected")

func is_collected():
	return $CollisionShape.disabled
