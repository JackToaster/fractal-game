tool
extends Spatial

export(float) var alpha setget set_alpha, get_alpha
export(Color) var glow_color setget set_glow_color, get_glow_color
export(float) var glow_size setget set_glow, get_glow


onready var material = $KIFS.get_surface_material(0)

func set_alpha(value: float):
	if material != null:
		material.set_shader_param("alpha", value)
	alpha = value

func get_alpha():
	return alpha

func set_glow_color(value: Color):
	if material != null:
		material.set_shader_param("glow", value)
	glow_color = value

func get_glow_color():
	return glow_color

func set_glow(value: float):
	if material != null:
		material.set_shader_param("glowSize", value)
	glow_size = value

func get_glow():
	return glow_size

func explode():
	$AnimationPlayer.play("explode")
	$AudioStreamPlayer3D.play()
