tool
extends "res://scripts/raymarched/RayMarchedGeom.gd"

export(float) var object_scale = 1.0
export(float) var fractal_scale = 3.0
export(float) var rotx = 1.0
export(float) var roty = 2.0
export(float) var rotz = 3.0

export(float) var smoothing = 0.05
export(int) var iterations = 20
export(Vector3) var offset = Vector3.ONE

func set_fractal_parameters():
	material.set_shader_param("objectScale", object_scale)
	material.set_shader_param("scale", fractal_scale)
	material.set_shader_param("rotx", rotx)
	material.set_shader_param("roty", roty)
	material.set_shader_param("rotz", rotz)
	material.set_shader_param("iterations", iterations)
	material.set_shader_param("smoothing", smoothing)
	material.set_shader_param("offset", offset)


func _makeRPY(roll: float, pitch: float, yaw: float):
	return Transform(Quat(Vector3(roll, pitch, yaw)))


func map(p: Vector3) -> float:
	p = global_transform.xform_inv(p)
	p = p / object_scale
	var rot = _makeRPY(rotx, roty, rotz)
	p = p * 0.4
	
	for _i in range(iterations):
		p = rot.xform(p)
		var tmp: float = 0.0
		p = Vector3(abs(p.x), abs(p.y), abs(p.z))
		
		if (p.x-p.y<0.0):
			tmp=p.y;p.y=p.x;p.x=tmp;
		if (p.x-p.z<0.0):
			tmp=p.z;p.z=p.x;p.x=tmp;
		if (p.y-p.z<0.0):
			tmp=p.z;p.z=p.y;p.y=tmp;
		
		p.z -= 0.5*offset.z*(fractal_scale-1.0)/fractal_scale;
		p.z = -abs(-p.z);
		p.z += 0.5*offset.z*(fractal_scale-1.0)/fractal_scale;

		p.x = fractal_scale*p.x - offset.x*(fractal_scale-1.0);
		p.y = fractal_scale*p.y - offset.y*(fractal_scale-1.0);
		p.z = fractal_scale*p.z;
	
	var d: Vector3 = Vector3(abs(p.x), abs(p.y), abs(p.z)) - Vector3.ONE
	var distance: float = min(max(d.x, max(d.y, d.z)),0.0) + Vector3(max(d.x,0.0),max(d.y,0.0), max(d.z,0.0)).length();
	distance *= pow(fractal_scale, -float(iterations))
	return distance * object_scale
