tool
extends "res://scripts/raymarched/RayMarchedGeom.gd"

export(float) var object_scale = 1.0

export(Vector3) var axis
export(float) var angle
export(int) var iterations = 16
export(float) var smoothing = 0.01
export(float) var fScale = 1.25
export(Vector3) var vOffset = Vector3(-1, -2, -0.2)

export(float) var sdf_offset = -0.4

func set_fractal_parameters():
	material.set_shader_param("objectScale", object_scale)
	material.set_shader_param("axis", axis)
	material.set_shader_param("angle", angle)
	material.set_shader_param("iterations", iterations)
	material.set_shader_param("smoothing", smoothing)
	material.set_shader_param("fScale", fScale)
	material.set_shader_param("vOffset", vOffset)
	

func _set_rot(q: Quat):
	return Transform(q)

func _set_rot2(vAxis: Vector3, fAngle: float):
	return _set_rot(Quat(vAxis.normalized(), fAngle))

export(float) var hollow_size = 2.0

func map(vPos: Vector3):
	vPos = global_transform.xform_inv(vPos)
	if vPos.length() < hollow_size:
		return 99999.0
	vPos = vPos / object_scale
	var m: Transform = _set_rot2(axis, angle)
	
	var total_scale: float = 1.0
	for _i in range(iterations):
		vPos = Vector3(abs(vPos.x), abs(vPos.y), abs(vPos.z))
		vPos *= fScale
		total_scale *= fScale
		vPos += vOffset
		vPos = m.xform(vPos)
	
	var l: float = vPos.length() / total_scale
	l = l - smoothing
	return l * object_scale + sdf_offset
