tool
extends "res://scripts/raymarched/RayMarchedGeom.gd"


export(float) var engine_strength = 0.0;
export(float) var collision_radius = 0.63

onready var engine_mat = $Engine.get_surface_material(0)
onready var engine_anim = $EngineAnimation

func _ready():
	._ready()
	#set_target_angle(Vector2.ZERO)

func _process(delta):
	._process(delta)
	check_collisions()
	update_engine_plume()

func update_engine_plume():
	engine_mat.set_shader_param("emission_energy", engine_strength)
	if not Engine.editor_hint:
		if GlobalSettings.game_root.time_state == Game.TimeState.bullet:
			engine_anim.playback_speed = 10
		else:
			engine_anim.playback_speed = 1

signal ship_collided
func check_collisions():
	var closest: float = INF
	var closest_obstacle
	var obstacles = get_tree().get_nodes_in_group("SDF")
	for obstacle in obstacles:
		var distance = obstacle.map(global_transform.origin)
		if distance < closest:
			closest = distance
			closest_obstacle = obstacle
	
#	print("Ship distance: " + str(closest))
	if closest < collision_radius:
		emit_signal("ship_collided", closest_obstacle)


func start_engine():
	engine_anim.play("start")

func stop_engine():
	engine_anim.play("stop")
