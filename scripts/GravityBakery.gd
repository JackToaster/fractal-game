extends Node
export(float) var integrator_timestep = 0.01
export(int) var bake_interval = 10
export(float) var bake_time_seconds = 100.0

func save_states(nodes: Array, time: float):
	for node in nodes:
		node.save_bake_state(time)

func bake_scene():
	var baked_nodes = get_tree().get_nodes_in_group("BakedGravitationalBody")
	var gravity_nodes = get_tree().get_nodes_in_group("GravitationalBody")
	print('Baking orbits for %d nodes out of %d gravity nodes' % [len(baked_nodes), len(gravity_nodes)])
	
	for node in baked_nodes:
		node.set_max_timestep(bake_time_seconds)
	
	save_states(baked_nodes, 0.0)
	for idx in range(0, bake_time_seconds / integrator_timestep):
		var bake_time = idx * integrator_timestep
		# Save state every n frames
		idx += 1
		if idx % bake_interval == 0:
			save_states(baked_nodes, bake_time / bake_time_seconds)
			#print('Saved at timestep %d' % idx)
		
		# Determine acceleration on each object
		for node in baked_nodes:
			for other in gravity_nodes:
				if node == other:
					continue
				else:
					var offset: Vector3 =  other.bake_position - node.bake_position
					var r_squared = offset.length_squared()
					var accel = GlobalSettings.gravitational_constant * other.mass / r_squared
					node.bake_acceleration = accel * offset.normalized()
		
		# Update velocity & position accordingly
		for node in baked_nodes:
			node.integrate_position(integrator_timestep)
	
