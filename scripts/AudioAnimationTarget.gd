extends Node

export(String) var muffle_effect_bus
export(Array, float) var muffled_eq_settings

export(float) var tween_time = 0.25

export(float, 0, 1) var muffled_ness setget set_muffledness, get_muffledness

func set_muffledness(value: float):
	muffled_ness = value
	var bus = AudioServer.get_bus_index(muffle_effect_bus)
	var eq_effect: AudioEffectEQ = AudioServer.get_bus_effect(bus, 0)
	for band_idx in range(eq_effect.get_band_count()):
		var setting: float
		if band_idx >= len(muffled_eq_settings):
			setting = lerp(0, muffled_eq_settings[-1], muffled_ness)
		else:
			setting = lerp(0, muffled_eq_settings[band_idx], muffled_ness)
		
		eq_effect.set_band_gain_db(band_idx, setting)

func get_muffledness():
	return muffled_ness

func slow_down():
	$MufflednessTween.interpolate_property(self, "muffled_ness", muffled_ness, 1.5, tween_time)
	$MufflednessTween.start()
	$SlowDownSound.play()
	$SpeedUpSound.stop()

func speed_up():
	$MufflednessTween.interpolate_property(self, "muffled_ness", muffled_ness, 0.0, tween_time)
	$MufflednessTween.start()
	$SpeedUpSound.play()
	$SlowDownSound.stop()
