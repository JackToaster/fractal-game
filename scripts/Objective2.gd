extends "res://scripts/raymarched/Objective.gd"

export(String) var hint_text
export(NodePath) var hint_text_nodepath

var can_be_collected: bool = false
onready var hint_text_node = get_node(hint_text_nodepath)
onready var old_hint_text = hint_text_node.text
func reset():
	$AnimationPlayer.play("pulse")
	$CollisionShape.disabled = false
	can_be_collected = false
	visible = false
	hint_text_node.text = old_hint_text

func collect():
	if can_be_collected:
		$AnimationPlayer.play("fadeout")
		$AudioStreamPlayer3D.pitch_scale = rand_range(0.75, 1.5)
		$AudioStreamPlayer3D.play()
		$CollisionShape.disabled = true
		emit_signal("objective_collected")

func is_collected():
	return $CollisionShape.disabled

func _on_Objective_objective_collected():
	can_be_collected = true
	visible = true
	hint_text_node.text = hint_text
	hint_text_node.get_node("AnimationPlayer").play("ShowHint")
