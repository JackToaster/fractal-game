tool
extends ColorRect

export(float) var strength setget set_strength, get_strength

func set_strength(value: float):
	material.set_shader_param("alpha", value)
	strength = value

func get_strength():
	return strength


func fade_in():
	$WarpAnimation.play("FadeIn")
func fade_out():
	$WarpAnimation.play("FadeOut")


signal fade_in_complete
signal fade_out_complete
func _on_WarpAnimation_animation_finished(anim_name):
	if anim_name == "FadeIn":
		emit_signal("fade_in_complete")
	elif anim_name == "FadeOut":
		emit_signal("fade_out_complete")
