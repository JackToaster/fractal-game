extends Spatial
class_name Level

export(NodePath) var camera_start_node
onready var camera_start: Spatial = get_node(camera_start_node)

export(NodePath) var camera_preview_node
onready var camera_preview: Spatial= get_node(camera_preview_node)

export(NodePath) var camera_postview_node
onready var camera_postview: Spatial= get_node(camera_postview_node)

export(float) var camera_preview_speed = 1.0
export(float) var preview_time = 5.0
export(float) var camera_reset_speed = 2.0
export(float) var reset_time = 2.0
export(float) var camera_postview_speed = 0.5
export(float) var win_time = 1.0
export(float) var won_time = 2.0

export(float) var explode_time = 1.0

export(NodePath) var player_node
onready var player = get_node(player_node)

export(float) var level_time: float = 0.0

enum State{
	init,
	preview,
	resetting,
	playing,
	winning,
	won,
	exploding
}

var state = State.init

onready var camera = $InterpolatedCamera
onready var state_timer = $Timer

onready var player_start_transform = player.transform
onready var player_start_vel = player.linear_velocity

# Called when the node enters the scene tree for the first time.
func _ready():
	player.ship.connect("ship_collided", self, "_on_ship_collided")

func update_state(new_state):
	match new_state:
		State.preview:
			$WarpEffect.fade_out()
			camera.global_transform = camera_start.global_transform
			get_tree().paused = true
			camera.target = camera_preview.get_path()
			camera.speed = camera_preview_speed
			
			$Text/LevelTitle/AnimationPlayer.play("ShowTitle")
			state_timer.start(preview_time)
		State.resetting:
			get_tree().paused = true
			get_tree().call_group("objective", "reset")
			
			GlobalSettings.game_root.set_time_state(Game.TimeState.normal)
			
			var cam_trans = camera.global_transform
			player.transform = player_start_transform
			player.linear_velocity = player_start_vel
			camera.global_transform = cam_trans
			
			camera.target = player.camera_target.get_path()
			camera.speed = camera_reset_speed
			
			player.hide_trajectory()
			
			$Tween.interpolate_property(self, "level_time", level_time, 0, reset_time)
			$Tween.start()
			
			$Text/HintText/AnimationPlayer.play("ShowHint")
			
			state_timer.start(reset_time)
		State.playing:
			get_tree().paused = false
			camera.target = player.camera_target.get_path()
			player.show_trajectory()
		State.winning:
			state_timer.start(win_time)
			player.hide_trajectory()
		State.won:
			$WarpEffect.fade_in()
			get_tree().paused = true
			var camera_trans = camera.global_transform
			player.remove_child(camera)
			add_child(camera)
			camera.global_transform = camera_trans
			camera.target = camera_postview.get_path()
			camera.speed = camera_postview_speed
			state_timer.start(won_time)
		State.exploding:
			player.explode()
			GlobalSettings.game_root.set_time_state(Game.TimeState.normal)
			get_tree().paused = true
			state_timer.start(explode_time)
		_:
			pass
	state = new_state

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print("State: " + str(state))
	match state:
		State.init:
			GravityBakery.bake_scene()
			update_state(State.preview)
			
			for objective in get_tree().get_nodes_in_group("objective"):
				objective.connect("objective_collected", self, "_on_objective_collected")
		State.playing:
			if !get_tree().paused:
				level_time += delta
				if GlobalSettings.game_root.time_state == Game.TimeState.bullet:
					camera.speed = player.bullettime_camera_speed
				else:
					camera.speed = player.camera_speed
				get_tree().call_group("BakedGravitationalBody", "interpolate", level_time)
				get_tree().call_group("Trajectory", "set_time", level_time)
				
				if Input.is_action_just_pressed("reset"):
					update_state(State.resetting)
		State.resetting:
			get_tree().call_group("BakedGravitationalBody", "interpolate", level_time)
			get_tree().call_group("Trajectory", "set_time", level_time)
		_:
			pass


func _on_Timer_timeout():
	match state:
		State.preview:
			update_state(State.resetting)
			# Reparent the camera to the player to avoid jittering
			var camera_trans = camera.global_transform
			remove_child(camera)
			player.add_child(camera)
			camera.global_transform = camera_trans
			$Text/LevelTitle/AnimationPlayer.play("HideTitle")
		State.resetting:
			update_state(State.playing)
		State.winning:
			update_state(State.won)
		State.won:
			GlobalSettings.game_root.next_level()
		State.exploding:
			update_state(State.resetting)
			player.unexplode()
		_:
			pass


func _on_ship_collided(_obstacle):
	if state == State.playing or state == State.winning:
		update_state(State.exploding)

func _on_objective_collected():
	var objectives = get_tree().get_nodes_in_group("objective")
	var all_collected = true
	for objective in objectives:
		all_collected = all_collected and objective.is_collected()
	if all_collected:
		update_state(State.winning)
