extends Spatial

export(float) var mass = 1
export(float) var radius = 0

onready var bake_position = global_transform.origin
