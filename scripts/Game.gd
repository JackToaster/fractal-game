extends Spatial
class_name Game

export(float) var bullet_time_scale = 0.1
export(float) var bullet_time_transition = 0.3

export(int) var level_idx = 0
export(Array, PackedScene) var levels

export(String, FILE) var menu_scene

onready var time_tween = Tween.new()

enum TimeState{
	normal,
	bullet,
	frozen
}

var time_state = TimeState.normal

# Called when the node enters the scene tree for the first time.
func _ready():
	GlobalSettings.register_game_root(self)
	set_level(level_idx)

func next_level():
	level_idx += 1
	if level_idx >= len(levels):
		# WON!
		get_tree().paused = false
		set_time_state(TimeState.normal)
		get_tree().change_scene(menu_scene)
	else:
		set_level(level_idx)

func set_level(idx: int):
	var current_level_or_null = get_node_or_null("Level")
	if current_level_or_null != null:
		current_level_or_null.queue_free()
		remove_child(current_level_or_null)
	
	var level = levels[idx].instance()
	level.name = "Level"
	call_deferred("add_child", level)

func set_time_state(new_state):
	if new_state == time_state:
		return
	# Tween machine broke
#	# Can't tween when timescale is zero!
#	if time_state == TimeState.frozen and new_state != TimeState.frozen:
#		match new_state:
#			TimeState.normal:
#				Engine.time_scale = 1.0
#			TimeState.bullet:
#				Engine.time_scale = bullet_time_scale
#	else:
#		match new_state:
#			TimeState.normal:
#				set_time_scale_tween(1.0)
#			TimeState.bullet:
#				set_time_scale_tween(bullet_time_scale)
#			TimeState.frozen:
#				Engine.time_scale = 0.0
	match new_state:
		TimeState.normal:
			Engine.time_scale = 1.0
			if time_state == TimeState.bullet:
				$AudioAnimationTarget.speed_up()
		TimeState.bullet:
			Engine.time_scale = bullet_time_scale
			$AudioAnimationTarget.slow_down()
		TimeState.frozen:
			Engine.time_scale = 0.0
	
	time_state = new_state


# TODO: Fix this because it doesn't work
func set_time_scale_tween(new_time_scale: float):
	time_tween.interpolate_property(Engine, "time_scale", Engine.time_scale,
		new_time_scale, bullet_time_transition)
