extends Tween

func _process(delta):
	if Engine.time_scale != 0.0:
		playback_speed = 1.0 / Engine.time_scale

func _fixed_process(delta):
	if Engine.time_scale != 0.0:
		playback_speed = 1.0 / Engine.time_scale
