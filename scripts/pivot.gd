extends Spatial

export(Vector3) var euler_rotation

onready var rotation_quat = Quat(euler_rotation)

var rotation_time = 0.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotation_time += delta
	global_transform.basis = Basis(Quat.IDENTITY.slerpni(rotation_quat, rotation_time))
