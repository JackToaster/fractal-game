extends ImmediateGeometry

export(int) var num_points = 100
export(float) var integration_timestep = 0.02
export(int) var point_divisor = 20
export(float) var point_scale = 1.0
export(Curve) var weight_curve


const left_offset = Vector3(-1, 0, 0)
const right_offset = Vector3(1, 0, 0)


var time = 0.0

func _shell_theorem_newtonian_gravity_accel(var shell_mass, var shell_radius, var r):
	# If a body is inside a spherically symmetric mass, that mass will not apply a net force on the body.
	# This is to only count the mass inside of the shell
	if r < shell_radius:
		shell_mass = pow(r / shell_radius, 3) * shell_mass
	
	return GlobalSettings.gravitational_constant * shell_mass / (r * r)


func set_time(time: float):
	self.time = time

func _ready():
	pass

func _process(_delta):
	var camera_position: Vector3 = get_viewport().get_camera().global_transform.origin
	var points = _integrate_position(camera_position)
	_draw(points)

func _integrate_position(camera_pos):
	# Get the nodes that will apply gravity to the ship
	var baked_nodes = get_tree().get_nodes_in_group("BakedGravitationalBody")
	var fixed_gravity_nodes = get_tree().get_nodes_in_group("FixedGravitationalBody")
	
	var points = []
	
	var position = global_transform.origin
	var velocity = get_parent().linear_velocity
	
	for i in range(num_points * point_divisor):
		var step_time = time + i * integration_timestep
		
		var net_acceleration = Vector3.ZERO
		
		for node in baked_nodes:
			var node_pos = node.get_interpolated_position(step_time)
			var offset = node_pos - position
			var accel = _shell_theorem_newtonian_gravity_accel(node.mass, node.radius, offset.length())
			net_acceleration += offset.normalized() * accel
		
		for node in fixed_gravity_nodes:
			var node_pos = node.global_transform.origin
			var offset = node_pos - position
			var accel = _shell_theorem_newtonian_gravity_accel(node.mass, node.radius, offset.length())
			net_acceleration += offset.normalized() * accel
		
		position += velocity * integration_timestep + net_acceleration * integration_timestep * integration_timestep
		velocity += net_acceleration * integration_timestep
		
		if i % point_divisor == 0:
			var camera_dir = (camera_pos - position).normalized()
			var velocity_dir = velocity.normalized()
			var point_trans = Transform.IDENTITY.looking_at(velocity_dir, camera_dir)
			point_trans.origin = position
			points.append(point_trans)
	
	return points

func _draw(points):
	# Clean up before drawing.
	clear()

	# Begin draw.
	begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	var global_rotation = global_transform.basis

	for i in range(num_points):
		var point = points[i]
		if point != null:
#			set_normal(global_rotation.xform_inv(point.basis.xform(Vector3.UP)))

			var uv_y = i / float(len(points))
			var curve_weight = weight_curve.interpolate_baked(1.0 - uv_y)

			set_uv(Vector2(0, uv_y))
			add_vertex(global_transform.xform_inv(point.xform(left_offset * curve_weight)))

			set_uv(Vector2(1, uv_y))
			add_vertex(global_transform.xform_inv(point.xform(right_offset * curve_weight)))


	# End drawing.
	end()
