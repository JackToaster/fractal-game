extends Spatial

export(float) var angle_damping = 0.8

var target_rotation: Quat = Quat(Vector3(0, 0, 0))

func _physics_process(_delta):
	update_angle()

func set_target_angle(target: Vector2):
#	rotation.x = target.x - 90.0
#	rotation.y = target.y
	target_rotation = Quat(Vector3(target.x, target.y, 0.0))

func update_angle():
	var current_rotation: Quat = Quat(transform.basis)
	transform.basis = Basis(target_rotation.slerp(current_rotation, angle_damping))
