extends Node

export(bool) var cast_ship_shadows = true

export(float) var default_resolution_scaling = 1

export(Curve) var test

const gravitational_constant = 5.0

var game_root: Game

func _ready():
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_DISABLED,SceneTree.STRETCH_ASPECT_IGNORE, Vector2(320, 240), default_resolution_scaling)

func register_game_root(game: Game):
	game_root = game

func has_game_root():
	return game_root != null
