extends Spatial

# How far off of the camera angle the ship should point
export(float) var pointing_angle_offset = 0.25

export(float) var thrust = 1.0
export(float) var bullet_time_thrust = 5.0

export(float) var camera_speed = 8.0
export(float) var bullettime_camera_speed = 25.0

export(bool) var fixed_gravity = false
export(Vector3) var fixed_gravity_vector = Vector3.DOWN

export(float) var pointing_dir_damping = 0.98

export(float) var mass = 1.0
export(Vector3) var linear_velocity

onready var other_bodies: Array = get_tree().get_nodes_in_group("GravitationalBody")

onready var ship = $ShipContainer/ShipPivot/Ship
onready var ship_pivot = $ShipContainer/ShipPivot
onready var ship_container = $ShipContainer
onready var camera = $ShipContainer/CameraPivot
onready var camera_target = $ShipContainer/CameraPivot/CameraTarget

onready var gravity_direction: Vector3 = -ship.global_transform.basis.y

# Called when the node enters the scene tree for the first time.
#func _ready():


# Newton's law of universal gravitation
# F = GMm/r^2
func _newtonian_gravity(m1: float, m2: float, r: float):
	return GlobalSettings.gravitational_constant * m1 * m2 / (r * r)

func _shell_theorem_newtonian_gravity(var m1, var shell_mass, var shell_radius, var r):
		# If a body is inside a spherically symmetric mass, that mass will not apply a net force on the body.
	# This is to only count the mass inside of the shell
	if r < shell_radius:
		shell_mass = pow(r / shell_radius, 3) * shell_mass
	
	return GlobalSettings.gravitational_constant * shell_mass * m1 / (r * r)

func _physics_process(delta):
	# Determine forces from gravity
	var total_gravity_vector = Vector3.ZERO
	for body in other_bodies:
		if body == self:
			continue
		var offset: Vector3 = body.global_transform.origin - global_transform.origin
		var force: float = _shell_theorem_newtonian_gravity(mass, body.mass, body.radius, offset.length())
		
		var force_vector: Vector3 = offset.normalized() * force
		total_gravity_vector += force_vector
	
	
	
	# Don't change orientation while in bullet time mode!
	if GlobalSettings.game_root.time_state == Game.TimeState.normal and linear_velocity.length() > 0:
		var look_dir = linear_velocity.normalized()
		# Don't try to rotate if the direction is indeterminite
		if (look_dir + gravity_direction).length() > 0.01:
			# Make ship point in the local upwards direction based on gravity
			var target_rotation = Transform.IDENTITY.looking_at(look_dir, -gravity_direction).basis
			#= global_transform.looking_at(origin + linear_velocity, -gravity_direction).basis
			ship_container.global_transform.basis = target_rotation.slerp(ship_container.global_transform.basis, pointing_dir_damping).orthonormalized()
	
	# Apply engine thrust
	if Input.is_action_pressed("thrust"):
		if GlobalSettings.game_root.time_state == Game.TimeState.bullet:
			total_gravity_vector += ship.global_transform.basis.y * bullet_time_thrust
		else:
			total_gravity_vector += ship.global_transform.basis.y * thrust
	
	# If space is held, point forwards
	if Input.is_action_pressed("aim"):
		ship_pivot.set_target_angle(camera.get_camera_rotation() + Vector2(pointing_angle_offset, 0.0))
		GlobalSettings.game_root.set_time_state(Game.TimeState.bullet)
	else:
		ship_pivot.set_target_angle(Vector2.ZERO)
		GlobalSettings.game_root.set_time_state(Game.TimeState.normal)
	
	
	# Integrate velocity/position
	var acceleration = total_gravity_vector / mass
	global_transform.origin += linear_velocity * delta + acceleration * delta * delta * 0.5
	linear_velocity += acceleration * delta
	
	if fixed_gravity:
		gravity_direction = fixed_gravity_vector.normalized()
	else:
		gravity_direction = total_gravity_vector.normalized()
		#print(gravity_direction)

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			ship.start_engine()
		else:
			ship.stop_engine()

func explode():
	$Explosion.explode()
	ship.visible = false

func unexplode():
	# Makes ship visible again if it exploded
	ship.visible = true

func reset():
	ship_pivot.transform = Transform.IDENTITY
	camera.rotation.x = 0.0
	camera.rotation.y = 0.0


func _on_ShipCollision_area_entered(area: Area):
	if area.is_in_group("objective"):
		area.collect()

func show_trajectory():
	$Trajectory.visible = true

func hide_trajectory():
	$Trajectory.visible = false


# Explosion animation done
func _on_AnimationPlayer_animation_finished(anim_name):
#	ship.visible = true
	pass
