extends Object
class_name Vector3Curve

var xcurve
var ycurve
var zcurve

func _init():
	xcurve = Curve.new()
	xcurve.min_value = -1024.0
	xcurve.max_value = 1024.0
	ycurve = Curve.new()
	ycurve.min_value = -1024.0
	ycurve.max_value = 1024.0
	zcurve = Curve.new()
	zcurve.min_value = -1024.0
	zcurve.max_value = 1024.0

func set_max_value(max_value: float):
	xcurve.max_value = max_value
	ycurve.max_value = max_value
	zcurve.max_value = max_value

func clear_points():
	xcurve.clear_points()
	ycurve.clear_points()
	zcurve.clear_points()

func add_point(time: float, point: Vector3):
	xcurve.add_point(Vector2(time, point.x), 0, 0, 1, 1)
	ycurve.add_point(Vector2(time, point.y), 0, 0, 1, 1)
	zcurve.add_point(Vector2(time, point.z), 0, 0, 1, 1)

func interpolate(time: float) -> Vector3:
	return Vector3(xcurve.interpolate(time), ycurve.interpolate(time), zcurve.interpolate(time))
