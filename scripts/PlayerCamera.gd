extends Spatial

export(float) var view_rotation_speed = 0.01
export(float) var zoom_min = 2.0
export(float) var zoom_max = 8.0
export(float) var zoom_factor = 1.1

onready var target_node = $CameraTarget
onready var zoom_dist: float = target_node.transform.origin.z

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _rotate_camera(amount: Vector2):
	rotation.y -= amount.x * view_rotation_speed
	rotation.x = clamp(rotation.x - amount.y * view_rotation_speed, deg2rad(-89), deg2rad(89))

func _zoom_camera():
	target_node.transform.origin = Vector3(0, 0, zoom_dist)

func _zoom_in():
	zoom_dist = clamp(zoom_dist / zoom_factor, zoom_min, zoom_max)
	_zoom_camera()

func _zoom_out():
	zoom_dist = clamp(zoom_dist * zoom_factor, zoom_min, zoom_max)
	_zoom_camera()

func _input(event):
	if event is InputEventMouseMotion:
		var movement = event.relative
		_rotate_camera(movement)
	elif event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_WHEEL_UP:
			_zoom_in()
		elif event.button_index == BUTTON_WHEEL_DOWN:
			_zoom_out()

# Returns the target angle the camera is pointing to
func get_camera_rotation():
	return Vector2(rotation.x, rotation.y)
